<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head profile="http://www.w3.org/2005/10/profile">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Friends Of Barefoot College USA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon2.png">


    <link rel="stylesheet" href="static/css/main.min.css">
    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="static/js/main.min.js"></script>
</head>
<body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <div class="container">
                <nav>
                    <img src="static/img/logo.png" alt="logo" class="logo" />
                    <div class="right">

                        <a href="http://barefootcollege.org/" class="goto" target="_blank">Go To Barefoot College <span>India</span></a>
                        <div class="social-media">
                            <a href="https://www.facebook.com/BarefootCollege" target="_blank">
                                <img src="static/img/icons/Facebook.svg" alt="facebook" />
                            </a>
                            <a href="https://twitter.com/Barefootcollege/" target="_blank">
                                <img src="static/img/icons/Twitter.svg" alt="twitter" />
                            </a>
                            <a href="http://wp.barefootcollege.org/contact-us/" target="_blank">
                                <img src="static/img/icons/Mailsvg.svg" alt="mail" />
                            </a>
                        </div>
                        <a href="https://www.barefootcollege.org/donate/" class="donate" target="_blank">Donate</a>
                    </div>
                </nav>

                <div id="slides">
                    <a href="https://www.barefootcollege.org/donate/" target="_blank"><img src="static/img/holiday-banner.jpg" alt="banner" /></a>
                    <img src="static/img/banner.jpg" alt="banner" />
                </div>



                <div class="section cream">
                    <h1>About Us</h1>

                    <p>Friends of Barefoot College (FOBC) is a USA-based organization designed to champion the work and values of barefoot education internationally. As developed by Barefoot College in India, barefoot education is is the transformation of rural communities through the empowerment of women. By applying rural wisdom and knowledge, the Barefoot approach demystifies and decentralizes sophisticated technology and believes that formal education is not required for women to bring renewable energy and entrepreneurial activities to their communities. This approach has been proven and tested over 40 years in 72 countries, touching more than 500,000 lives. </p>
                </div>

                <div class="section">
                    <h1>Our Programs</h1>

                    <p>Our primary purpose is to expand barefoot education globally.  Friends of Barefoot College, USA specifically aims to provide financial support for the development of barefoot education programs (or regional barefoot education centers) throughout the world.  Your contribution means funding for global programs such as</p>


                    <div class="projects">
                            <div class="project">
                                <img src="static/img/Project4_big.jpg" alt="banner" />
<!--                                 <iframe width="420" height="315" src="//www.youtube.com/embed/XllIQl81aE0" frameborder="0" allowfullscreen></iframe> -->
                                <h2>Barefoot Opens Its First Of Six African Solar Training Centres</h2>
<p>The 13 original women trained as Women Barefoot Solar Engineers; came back to Zanzibar transformed, ready to teach and share their knowledge and skills . They have created a revolution that is bringing renewable energy and clean light, to communities form Kandwi, to Makanduchi, form Matemwe to Panza Island in Pemba. The Barefoot College Zanzibar is a true community college, providing an ongoing opportunity to show the world the essential enabler that access to basic lighting is, for the education of children, health and well-being of communities and the possibility of increasing economic horizons.</p><br>
<p>Visit them at their new website: <a href="http://barefootcollege-zanzibar.org/">barefootcollege-zanzibar.org</a></p>
<!--
                                <p>
                                    Sierra Leone and Liberia: These two countries are experiencing the most deadly outbreak of Ebola. The Women Barefoot Solar Engineers in these countries are equipped with the knowledge and skills to build resilience in response to the outbreak. <br/><br/>
                                    FOBC is supporting Women Barefoot Solar Engineers to provide solar lanterns and mini solar power plants to diagnostic clinics for those being tested for Ebola AND women/children with health issues not related to Ebola, currently suffering from the lack of healthcare infrastructure.   <br/><br/>
                                    The Barefoot Women Solar Engineers are constructing lanterns in these countries for household distribution. We are delivering clean light sustainably, so that children who have stopped going to school can learn, household cooking and hygiene can be improved, and livelihood activities by women may continue into the evening hours.
                                </p>
-->
                            </div>

                        <div class="project">
                            <img src="static/img/Project2_big.jpg" alt="banner" />
                            <h2>Barefoot College International Expansion of Regional Vocational Training Centers</h2>
                            <p>
                                In light of the 2014 Clinton Global Initiative, Barefoot College launched a commitment to open six regional vocational training centers in Africa, further expanding its training to illiterate/semi-literate women to become Barefoot Solar Engineers and enabling them to solar electrify their communities. This demystification of technology skills is coupled with a strong focus on enterprise development, financial inclusion skills, and women’s empowerment.  The commitment expands and replicates Barefoot College’s proven Solar Initiative model to Burkina Faso, Liberia, Senegal, South Sudan, Zanzibar, and Tanzania. The centers will be modeled on the College’s campus in Tilonia, India, where women are taught to build, repair, and maintain solar home lighting systems, lanterns and cookers. </p><br/>
                                <p>
                                    This initiative is based on an extraordinary partnership model bringing governments, social entrepreneurs, and private sector partners together. Through this partnership, we will catalyze a decentralized community-managed, sustainable, and green energy solution to lighting rural, non-electrified households and fostering women as environmental stewards and entrepreneurs.
                                </p>
                            </div>

                            <div class="project">
                                <img src="static/img/Project1_big.jpg" alt="banner" />
                                <h2>Barefoot Education Programs for Children</h2>
                                <p>This program draws on wisdom and strengths of communities by training local Barefoot teachers, who are selected to teach at Solar Powered Night Schools and day schools/crèches that cater to early and primary education. The Night Schools are tailored to provide education to children who otherwise would be excluded from school, namely young girls. The Rural Crèches and Day Schools are set up for the convenience of working mothers, so that they can leave their children in a safe and engaging environment. The Barefoot approach encourages that education in these schools focuses on raising awareness about social, economic, environmental, and political issues, and that literacy is only one part of education. </p>
                            </div>

                            <div class="project">
                                <img src="static/img/Project3_big.jpg" alt="banner" />
                                <h2>Resilience Building and Response Initiative</h2>
                                <p>Friends of Barefoot College, USA is committed to further empowering and supporting the Women Barefoot Solar Engineers after they return home - especially when these incredible women and the skills they have mastered, can be leveraged during health, humanitarian, and environmental crisis.</p>
                            </div>

                            <br style="display:block" /><br/>
                            <p class="left">

                                Please check back for updates on the progress of our three main global projects, especially our Clinton Global Initiative Commitment.
                            </p>

                            <br style="display:block" /><br/><br/>

                            <a href="https://www.barefootcollege.org/donate/" class="orange-btn donate-big">Donate to our projects</a>
                        </div>
                        <br style="display:block" />
                        <p class="footnote">
                            Barefoot College is a 40 year old not-for-profit entity registered in India. In order to create a way for USA based donors to support barefoot's missions, Friends of Barefoot College International USA, a 501c3 was created to support specific projects in Barefoot College's global expansion activities.
                        </p>
                    </div>

                    <footer>
                        <div class="left">
                            <img src="static/img/logo.png" alt="logo" /><br/>
                            <span class="copy">2014 &copy; All right reserved | <a href="#" target="_blank">Credits</a>
                            </div>

                            <div class="right">
                                <div class="social-media">
                                    <!-- pinterest -->
                                    <!-- facebook -->
                                    <!-- Twitter -->
                                    <!-- Youtube -->
                                    <!-- Email -->
                                    <a href="http://pinterest.com/barefootcollege/" target="_blank">
                                        <img src="static/img/icons/social_pinterest.png" alt="pinterest" />
                                    </a>
                                    <a href="https://www.facebook.com/BarefootCollege" target="_blank">
                                        <img src="static/img/icons/Facebook.svg" alt="facebook" />
                                    </a>
                                    <a href="https://twitter.com/Barefootcollege/" target="_blank">
                                        <img src="static/img/icons/Twitter.svg" alt="twitter" />
                                    </a>
                                    <a href="http://www.youtube.com/user/barefootcollege" target="_blank">
                                        <img src="static/img/icons/social_youtube.png" alt="youtube" />
                                    </a>
                                    <a href="http://wp.barefootcollege.org/contact-us/" target="_blank">
                                        <img src="static/img/icons/Mailsvg.svg" alt="mail" />
                                    </a>
                                </div>
                                <div class="newsletter">

                                    <!-- Begin MailChimp Signup Form -->
                                    <div id="mc_embed_signup">
                                        <form action="//barefootcollege.us6.list-manage.com/subscribe/post?u=914aa1e89d1029b930a040398&id=0de15d6409" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                            <div id="mc_embed_signup_scroll">
                                                <div class="mc-field-group">
                                                    <span>Newsletter</span><input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"/> <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"/>
                                                </div>
                                                <div id="mce-responses" class="clear">
                                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                                <div style="position: absolute; left: -5000px;"><input type="text" name="b_914aa1e89d1029b930a040398_71ca6ef754" tabindex="-1" value=""></div>
                                            </div>
                                        </form>
                                    </div>

                                    <!--End mc_embed_signup-->

                            <!--
                            <span>Newsletter</span>
                            <input type="text" />
                            <button>Subscribe</button>-->
                        </div>
                    </div>

                </footer>



            </div>

            <script type="text/javascript">
            /*  Homepage slide initialization */
            $(function(){
              $("#slides").slidesjs({
                width: 1280,
                height: 464,
                play: {
                      active: false,
                      effect: "slide",
                      interval: 8000,
                      auto: true,
                      swap: true,
                      pauseOnHover: false
                    }
            });
          });
            </script>

            <!-- <script src="js/main.js"></script> -->

        </body>
        </html>
